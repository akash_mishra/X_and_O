var socket=io.connect('http://localhost:3000/');
var canvas = document.getElementById('myCanvas');
var ctx;
var x;
var y;


var counter = 0;
var block11x = false;
var block12x = false;
var block13x = false;
var block21x = false;
var block22x = false;
var block23x = false;
var block31x = false;
var block32x = false;
var block33x = false;
var block11o = false;
var block12o = false;
var block13o = false;
var block21o = false;
var block22o = false;
var block23o = false;
var block31o = false;
var block32o = false;
var block33o = false;
var block11=true;
var block12=true;
var block13=true;
var block21=true;
var block22=true;
var block23=true;
var block31=true;
var block32=true;
var block33=true;
var player1Score=0;
var player2Score=0;
var player1Won=false;
var player2Won=false;


var showWinnerscreen=false;
window.onload = function() {

  ctx = canvas.getContext('2d');

  canvas.addEventListener('mousedown', coords);

  drawEverything();



}

function drawEverything() {
  ctx.fillStyle = '#3C3B5C';
  ctx.fillRect(0, 0, canvas.width, canvas.height);


  var playground = new Image();
  playground.src = "assets/playground.svg";
  playground.onload = function() {
    ctx.drawImage(playground, 0, 0, canvas.width, canvas.height);
  }

  //////////////////////////////////////////////////////////////////////////


}

function draw(a,b,counter){
	
	if(counter%2==1){
	drawX(a,b)
}
else{
	drawO(a,b);
}
}



function coords(e) {
  x = e.clientX;
  y = e.clientY;
  x -= canvas.offsetLeft;
  y -= canvas.offsetTop;
  console.log(x, y);
  counter++;
  console.log("counter=" + counter);

 
  //Listen from server///////////////////////////////////////////////////////////////////////////////

 Client.draw(x,y,counter);




  if (x < 200 && y < 200 && block11) {
        block11=false;

  
    if (counter % 2 == 1) {
      drawX(10,10);

       block11x = true;
    } else  {
      drawO(10,10);
       block11o=true;

    }

    }



  if (x > 200 && x < 400 && y < 200 && block12) {
    block12=false;

    if (counter % 2 == 1) {
      drawX(210,10);

       block12x = true;
    } else  {
      drawO(210,10);
       block12o=true;

    }

  }

  if (x > 400 && y < 200 && block13) {
    block13=false;

    if (counter % 2 == 1) {
      drawX(410,10);
      block13x = true;
    } else {
      drawO(410,10);

      block13o=true;
}
  }

  if (y > 200 && y < 400 && x < 200 && block21) {

    block21=false;

    if (counter % 2 == 1) {
      drawX(10,210);
      block21x = true;
    } else {
      drawO(10,210);
      block21o = true;
    }
  }

  if (y > 200 && y < 400 && x > 200 && x < 400 && block22) {

    block22=false;

    if (counter % 2 == 1) {
      drawX(210,210);
      block22x = true;
    } else {
      drawO(210,210);
      block22o = true;
    }
  }

  if (y > 200 && y < 400 && x > 400 && block23) {
    block23=false;

    if (counter % 2 == 1) {
      drawX(410,210);
      block23x = true;
    } else {
      drawO(410,210);
      block23o = true;
    }
  }

  if (y > 400 && x < 200 && block31) {
    block31=false;

    if (counter % 2 == 1) {
      drawX(10,410);
      block31x = true;
    } else {
      drawO(10,410);

      block31o = true;
    }
  }
  if (y > 400 && x > 200 && x < 400 && block32) {
  	    	block32=false;

    if (counter % 2 == 1) {
      drawX(210,410);

      block32x = true;
    } else {
      drawO(210,410);

      block32o = true;
    }
    

  }

  if (y > 400 && x > 400 && block33) {
    block33=false;

    if (counter % 2 == 1) {
      drawX(410,410);

      block33x = true;
    } else {
      drawO(410,410);

      block33o = true;
    }
  }
    showWinner();
  }



function showWinner(){
    if((block11x && block12x && block13x) ||
    (block11x && block22x && block33x)||
    (block11x && block22x && block33x)||
    (block11x && block21x && block31x)||
    (block21x && block22x && block23x)||
    (block31x && block32x && block33x)||
    (block12x && block22x && block32x)||
    (block13x && block23x && block33x)||
    (block13x && block22x && block31x)){
      console.log("x wins");
   player1Score+=1;
player1Won=true;
   Client.won(player1Won,player2Won);
      player1Wins();
///   document.getElementById('player1').innerHTML=player1Score;



        }


else if((block11o && block12o && block13o)||
(block11o && block22o && block33o)||
(block11o && block22o && block33o)||
(block11o && block21o && block31o)||
(block21o && block22o && block23o)||
(block31o && block32o && block33o)||
(block12o && block22o && block32o)||
(block13o && block23o && block33o)||
(block13o && block22o && block31o)){
  console.log("o wins");
  player2Score+=1;
  player2Won=true;
 player2Wins();
   Client.won(player1Won,player2Won);

    //  document.getElementById('player2').innerHTML=player2Score;


}

}

function drawX(x1,y1)
{
  var img = new Image;
  img.src = 'assets/x.svg';
  img.onload = function() {
    ctx.drawImage(img,x1,y1, 150, 150);
};
}
function drawO(x1,y1){
  var img = new Image;
  img.src = 'assets/o.svg';
  img.onload = function() {
    ctx.drawImage(img,x1,y1, 150, 150);
  };
}

function player1Wins(){
	var img=new Image;
      img.src="assets/p1w.svg";
   img.onload=function(){ ctx.drawImage(img,0,0,canvas.width,canvas.height);}
}
function player2Wins(){
	 var img=new Image;
       img.src="assets/p2w.svg";
   img.onload=function(){ ctx.drawImage(img,0,0,canvas.width,canvas.height);}
}

function won(player1,player2){

	if(player1){
		player1Wins();
	}
	else if(player2){
		player2Wins();
	}

}