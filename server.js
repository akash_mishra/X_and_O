
var express=require('express');
var socket=require('socket.io');



 var app=express();
 var server = app.listen(3000,function () {
   console.log("Listening to 3000");
 });

//Static Files
app.use(express.static('public'));
//socket setup
var io=socket(server);
io.on('connection',function (socket) {
  //console.log('connection made',socket.id);


socket.on('draw',function(x,y,counter){
	socket.broadcast.emit('sum',x,y,counter);
		//io.emit('draw',x,y);//this emits function draw to all CLients
	//socket.emit('something');//this will emit to only that socket in which it is called
	//socket.broadcast.emit()//this will emit to all clients except one which has called it

});

socket.on('winner',function(winner1,winner2){
	socket.broadcast.emit('won',winner1,winner2);
});

});
